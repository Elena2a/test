package com.mycompany.l01;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class LoginTest extends FirstTest{

    @Test
    public void loginNegative() {
        //login field
        WebElement userLoginName = driver.findElement(By.id("UserName"));
        userLoginName.sendKeys("eabaeva");
        //pw field
        WebElement userPasswordField = driver.findElement(By.id("Password"));
        userPasswordField.sendKeys("Invalid password");
        //click to login button
        driver.findElement(By.id("logonButton")).click();

        assertTrue(driver.findElement(By.xpath("//div[@class='alert alert-error']"))
                        .getText().contains("Ошибка"));
    }

    @Test
    public void loginPositive() {
        WebElement userName = driver.findElement(By.id("UserName"));
        userName.clear();
        userName.sendKeys("eabaeva");

        WebElement password = driver.findElement(By.id("Password"));
        password.clear();
        password.sendKeys("123456");

        driver.findElement(By.id("logonButton")).click();

        assertTrue(isElementPresent(By.className("tt-input")));
        assertEquals(driver.findElements(By.className("field-validation-error")).size(), 0);
    }

}
